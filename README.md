# Consulta productos.

****

### Descripción

Consulta productos asociados al grupo empresarial inditex.


## Tecnologías empleadas

______________________

> - Java 17 
>- Lombok
>- Maven
>- Git
>- h2

## Instalación aplicación

______________________

Antes de empezar a utilizar el aplicativo debemos tener instalado los siguientes programas:

>- Java 1.17.x
>- maven 3.5.x
>- Git x.x.x


### Ejecutar aplicación

Para poder ejecutar el proyecto se debe ejecutar los dos siguientes comandos en la raiz del proyecto,

Compilación y validación de los test.

```bash
# Descargar dependencias y compilar proyecto.
mvn clean spotless:apply verify
```

Una vez finalice la correcta construcción del proyecto, ejecutarmos el siguiente comando para subir la app

```bash
# Ejecutar apliación
mvn spring-boot:run
```

## API Rest

****
El proyecto contiene una **colección de postman** para consumir el api y pruebas que verifican el correcto funcionamiento del servicio, 
se encuentran en la carpeta **/docs/postman**, se ecuentra la *colección y los test*.

### URIs de interes

> * **Salud aplicación:** /actuator/health
> * **Documentación api:** /v2/api-docs
> * **Swagger UI:** /swagger-ui/index.html
