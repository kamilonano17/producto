package com.inditex.product.business.service;

import com.inditex.product.business.dto.ProductPrice;
import com.inditex.product.business.exception.ProductPriceNotFoundException;
import com.inditex.product.business.port.intraestructure.ProductPriceFindPort;
import com.inditex.product.business.port.presentation.ProductPriceFindServicePort;
import com.inditex.product.business.port.presentation.command.ProductPriceFindByDateCommand;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class GetProductPriceService implements ProductPriceFindServicePort {

  private final ProductPriceFindPort productPriceFindPort;

  @Override
  public ProductPrice findPriceByDate(ProductPriceFindByDateCommand productPriceFindByDateCommand) throws ProductPriceNotFoundException {
    log.info("find product price, brandId: {}", productPriceFindByDateCommand);
    return productPriceFindPort.findProduct(productPriceFindByDateCommand.getProductId(), productPriceFindByDateCommand.getBrandId(),
        productPriceFindByDateCommand.getRequestDate()).orElseThrow(() -> new ProductPriceNotFoundException("Product price not found."));
  }

}
