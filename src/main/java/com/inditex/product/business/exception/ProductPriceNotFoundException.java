package com.inditex.product.business.exception;

public class ProductPriceNotFoundException extends RuntimeException {

  public ProductPriceNotFoundException(String message) {
    super(message);
  }

}
