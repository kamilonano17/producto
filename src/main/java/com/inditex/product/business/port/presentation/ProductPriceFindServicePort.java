package com.inditex.product.business.port.presentation;

import com.inditex.product.business.dto.ProductPrice;
import com.inditex.product.business.exception.ProductPriceNotFoundException;
import com.inditex.product.business.port.presentation.command.ProductPriceFindByDateCommand;

public interface ProductPriceFindServicePort {

  /**
   * Find product price that apply to request date.
   * 
   * @param productPriceFindByDateCommand Necessary data for find product price
   * @return Price of product that apply to request date
   * @throws ProductPriceNotFoundException If the price not found for the combination request date, product identifier and brand identifier.
   */
  ProductPrice findPriceByDate(ProductPriceFindByDateCommand productPriceFindByDateCommand) throws ProductPriceNotFoundException;

}
