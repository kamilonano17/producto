package com.inditex.product.business.port.intraestructure;

import java.time.LocalDateTime;
import java.util.Optional;

import com.inditex.product.business.dto.ProductPrice;

public interface ProductPriceFindPort {

  /**
   * find product price by date and brandId in database
   * 
   * @param productId Product identifier
   * @param brandId Brand identifier
   * @param requestDate Date of request.
   * @return product price that applies to the request date
   */
  Optional<ProductPrice> findProduct(Integer productId, Integer brandId, LocalDateTime requestDate);

}
