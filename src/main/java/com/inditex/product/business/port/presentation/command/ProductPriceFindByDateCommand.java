package com.inditex.product.business.port.presentation.command;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class ProductPriceFindByDateCommand {

  private Integer brandId;

  private Integer productId;

  private LocalDateTime requestDate;

}
