package com.inditex.product.business.dto;

import lombok.Getter;

@Getter
public enum Currency {
  EUR
}
