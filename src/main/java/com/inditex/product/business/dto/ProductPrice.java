package com.inditex.product.business.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ProductPrice {

  private Integer id;

  private Integer brandId;

  private Currency currency;

  private LocalDateTime endDate;

  private BigDecimal price;

  private Integer priceIdentifier;

  private Integer priority;

  private Integer productId;

  private LocalDateTime startDate;

}
