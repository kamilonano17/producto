package com.inditex.product.infraestructure.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inditex.product.business.dto.Currency;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "product_price")
public class ProductPriceEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "brand_id")
  private Integer brandId;

  @Column(name = "currency")
  @Enumerated(EnumType.STRING)
  private Currency currency;

  @Column(name = "end_date")
  private LocalDateTime endDate;

  @Column(name = "price")
  private BigDecimal price;

  @Column(name = "price_identifier")
  private Integer priceIdentifier;

  @Column(name = "priority")
  private Integer priority;

  @Column(name = "product_id")
  private Integer productId;

  @Column(name = "start_date")
  private LocalDateTime startDate;

}
