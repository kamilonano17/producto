package com.inditex.product.infraestructure.mapper;

import com.inditex.product.business.dto.ProductPrice;
import com.inditex.product.infraestructure.dto.ProductPriceEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductPriceEntityMapper {

  ProductPrice toProductPrice(ProductPriceEntity productPriceEntity);

}
