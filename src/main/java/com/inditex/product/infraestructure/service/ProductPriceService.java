package com.inditex.product.infraestructure.service;

import java.time.LocalDateTime;
import java.util.Optional;

import com.inditex.product.business.dto.ProductPrice;
import com.inditex.product.business.port.intraestructure.ProductPriceFindPort;
import com.inditex.product.infraestructure.mapper.ProductPriceEntityMapper;
import com.inditex.product.infraestructure.repository.ProductPriceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductPriceService implements ProductPriceFindPort {

  private final ProductPriceEntityMapper productPriceEntityMapper;
  private final ProductPriceRepository productPriceRepository;

  @Override
  public Optional<ProductPrice> findProduct(final Integer productId, final Integer brandId, final LocalDateTime requestDate) {
    return productPriceRepository.findByDate(brandId, productId, requestDate).map(productPriceEntityMapper::toProductPrice);
  }

}
