package com.inditex.product.infraestructure.repository;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Optional;

import com.inditex.product.infraestructure.dto.ProductPriceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductPriceRepository extends JpaRepository<ProductPriceEntity, BigInteger> {

  @Query("""
      SELECT p
      FROM ProductPriceEntity p
      WHERE p.brandId = :brandId AND p.productId = :productId AND p.startDate <= :requestDate AND p.endDate >= :requestDate
      order by p.priority desc
      limit 1
      """)
  Optional<ProductPriceEntity> findByDate(@Param("brandId") Integer brandId, @Param("productId") Integer productId,
      @Param("requestDate") LocalDateTime requestDate);

}
