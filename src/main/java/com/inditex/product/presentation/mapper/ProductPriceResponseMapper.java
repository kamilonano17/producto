package com.inditex.product.presentation.mapper;

import java.time.LocalDateTime;

import com.inditex.product.business.dto.ProductPrice;
import com.inditex.product.presentation.dto.ProductPriceResponse;
import org.mapstruct.Mapper;

@Mapper
public interface ProductPriceResponseMapper {

  ProductPriceResponse toProductPriceResponse(ProductPrice productPrice, LocalDateTime requestDate);

}
