package com.inditex.product.presentation.mapper;

import java.time.LocalDateTime;

import com.inditex.product.business.port.presentation.command.ProductPriceFindByDateCommand;
import org.mapstruct.Mapper;

@Mapper
public interface ProductPriceFindByDateCommandMapper {

  ProductPriceFindByDateCommand toProductPriceFindByDateCommand(Integer productId, Integer brandId, LocalDateTime requestDate);

}
