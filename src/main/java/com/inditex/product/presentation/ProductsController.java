package com.inditex.product.presentation;


import java.time.LocalDateTime;

import com.inditex.product.business.dto.ProductPrice;
import com.inditex.product.business.exception.ProductPriceNotFoundException;
import com.inditex.product.business.port.presentation.ProductPriceFindServicePort;
import com.inditex.product.presentation.api.ProductsApi;
import com.inditex.product.presentation.dto.ErrorApi;
import com.inditex.product.presentation.dto.ProductPriceResponse;
import com.inditex.product.presentation.mapper.ProductPriceFindByDateCommandMapper;
import com.inditex.product.presentation.mapper.ProductPriceResponseMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ProductsController implements ProductsApi {

  private final ProductPriceFindServicePort productPriceFindServicePort;
  private final ProductPriceResponseMapper productPriceResponseMapper;
  private final ProductPriceFindByDateCommandMapper productPriceFindByDateCommandMapper;

  @Override
  public ResponseEntity<ProductPriceResponse> getProductPrice(final Integer productId, final Integer brandId, final LocalDateTime requestDate) {
    log.info("GET /products/{}/brand/{}/price", productId, brandId);
    ProductPrice productPrice = productPriceFindServicePort
        .findPriceByDate(productPriceFindByDateCommandMapper.toProductPriceFindByDateCommand(productId, brandId, requestDate));
    return ResponseEntity.ok(productPriceResponseMapper.toProductPriceResponse(productPrice, requestDate));
  }

  @ExceptionHandler(ProductPriceNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ErrorApi responsePendingTransaction(ProductPriceNotFoundException exception) {
    return ErrorApi.builder().errorCode("PRICE_NOT_FOUND").errorMessage(exception.getMessage()).build();
  }

}
