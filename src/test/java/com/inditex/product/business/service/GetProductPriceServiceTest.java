package com.inditex.product.business.service;


import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.mockito.Mockito.when;

import java.util.Optional;

import com.inditex.product.business.dto.ProductPrice;
import com.inditex.product.business.dto.ProductPriceMother;
import com.inditex.product.business.exception.ProductPriceNotFoundException;
import com.inditex.product.business.port.intraestructure.ProductPriceFindPort;
import com.inditex.product.business.port.presentation.command.ProductPriceFindByDateCommand;
import com.inditex.product.business.port.presentation.command.ProductPriceFindByDateCommandMother;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@DisplayName("GetProductPriceService Tests")
@ExtendWith(MockitoExtension.class)
class GetProductPriceServiceTest {

  @Mock
  private ProductPriceFindPort productPriceFindPort;

  @InjectMocks
  private GetProductPriceService sut;

  @DisplayName("findPriceByDate tests")
  @Nested
  class FindPriceByDateTest {

    @DisplayName("GIVEN product price not found WHEN call get product price THEN throw ProductPriceNotFoundException")
    @Test
    void givenProductPriceNotFound_whenCallGetProductPrice_thenThrowProductPriceNotFoundException() {
      // GIVEN
      ProductPriceFindByDateCommand productPriceFindByDateCommand = ProductPriceFindByDateCommandMother.builder().defaultTemplate().build();
      ProductPriceNotFoundException expected = new ProductPriceNotFoundException("Product price not found.");

      when(productPriceFindPort.findProduct(productPriceFindByDateCommand.getProductId(), productPriceFindByDateCommand.getBrandId(),
          productPriceFindByDateCommand.getRequestDate())).thenReturn(Optional.empty());

      // WHEN
      ProductPriceNotFoundException result =
          catchThrowableOfType(() -> sut.findPriceByDate(productPriceFindByDateCommand), ProductPriceNotFoundException.class);

      // THEN
      assertThat(result).isNotNull().usingRecursiveComparison().isEqualTo(expected);
    }

    @DisplayName("GIVEN product price exist WHEN call get product price THEN return price")
    @Test
    void givenProductPriceExist_whenCallGetProductPrice_thenReturnPrice() {
      // GIVEN
      ProductPriceFindByDateCommand productPriceFindByDateCommand = ProductPriceFindByDateCommandMother.builder().defaultTemplate().build();
      ProductPrice expected = ProductPriceMother.builder().defaultTemplate().build();

      when(productPriceFindPort.findProduct(productPriceFindByDateCommand.getProductId(), productPriceFindByDateCommand.getBrandId(),
          productPriceFindByDateCommand.getRequestDate())).thenReturn(Optional.of(expected));

      // WHEN
      ProductPrice result = sut.findPriceByDate(productPriceFindByDateCommand);

      // THEN
      assertThat(result).isNotNull().usingRecursiveComparison().isEqualTo(expected);
    }

  }

}
