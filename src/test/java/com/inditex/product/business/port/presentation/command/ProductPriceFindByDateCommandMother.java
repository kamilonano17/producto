package com.inditex.product.business.port.presentation.command;

import java.time.LocalDateTime;

public class ProductPriceFindByDateCommandMother {

  // DEFAULT VALUES
  public static final Integer DEFAULT_PRODUCT_ID = 35455;
  public static final Integer DEFAULT_BRAND_ID = 1;
  public static final LocalDateTime DEFAULT_REQUEST_DATE = LocalDateTime.of(2020, 6, 14, 17, 0);

  // ATTRIBUTES
  private Integer productId;
  private Integer brandId;
  private LocalDateTime requestDate;

  public static ProductPriceFindByDateCommandMother builder() {
    return new ProductPriceFindByDateCommandMother();
  }

  // MODIFIERS
  public ProductPriceFindByDateCommandMother withProductId(Integer productId) {
    this.productId = productId;
    return this;
  }

  public ProductPriceFindByDateCommandMother withBrandId(Integer brandId) {
    this.brandId = brandId;
    return this;
  }

  public ProductPriceFindByDateCommandMother withRequestDate(LocalDateTime requestDate) {
    this.requestDate = requestDate;
    return this;
  }

  // TEMPLATES
  public ProductPriceFindByDateCommandMother defaultTemplate() {
    productId = DEFAULT_PRODUCT_ID;
    brandId = DEFAULT_BRAND_ID;
    requestDate = DEFAULT_REQUEST_DATE;
    return this;
  }

  public ProductPriceFindByDateCommand build() {
    //@off
    return ProductPriceFindByDateCommand.builder()
        .productId(productId)
        .brandId(brandId)
        .requestDate(requestDate)
        .build();
    //@on
  }

}
