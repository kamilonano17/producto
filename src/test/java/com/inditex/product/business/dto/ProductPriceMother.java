package com.inditex.product.business.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ProductPriceMother {
  // DEFAULT VALUES
  public static final Integer DEFAULT_ID = 35455;
  public static final Integer DEFAULT_BRAND_ID = 1;
  public static final Currency DEFAULT_CURRENCY = Currency.EUR;
  public static final LocalDateTime DEFAULT_END_DATE = LocalDateTime.of(2020, 12, 31, 23, 59, 59);
  public static final BigDecimal DEFAULT_PRICE = BigDecimal.valueOf(35.50);
  public static final Integer DEFAULT_PRICE_IDENTIFIER = 1;
  public static final Integer DEFAULT_PRIORITY = 0;
  public static final Integer DEFAULT_PRODUCT_ID = 1;
  public static final LocalDateTime DEFAULT_START_DATE = LocalDateTime.of(2020, 6, 14, 0, 0, 0);

  // ATTRIBUTES
  private Integer id;
  private Integer brandId;
  private Currency currency;
  private LocalDateTime endDate;
  private BigDecimal price;
  private Integer priceIdentifier;
  private Integer priority;
  private Integer productId;
  private LocalDateTime startDate;

  // CONSTRUCTOR
  public static ProductPriceMother builder() {
    return new ProductPriceMother();
  }

  // MODIFIERS
  public ProductPriceMother withId(Integer id) {
    this.id = id;
    return this;
  }

  public ProductPriceMother withBrandId(Integer brandId) {
    this.brandId = brandId;
    return this;
  }

  public ProductPriceMother withCurrency(Currency currency) {
    this.currency = currency;
    return this;
  }

  public ProductPriceMother withEndDate(LocalDateTime endDate) {
    this.endDate = endDate;
    return this;
  }

  public ProductPriceMother withPrice(BigDecimal price) {
    this.price = price;
    return this;
  }

  public ProductPriceMother withPriceIdentifier(Integer priceIdentifier) {
    this.priceIdentifier = priceIdentifier;
    return this;
  }

  public ProductPriceMother withPriority(Integer priority) {
    this.priority = priority;
    return this;
  }

  public ProductPriceMother withProductId(Integer productId) {
    this.productId = productId;
    return this;
  }

  public ProductPriceMother withStartDate(LocalDateTime startDate) {
    this.startDate = startDate;
    return this;
  }

  // TEMPLATES OF MOTHER

  /**
   * Default template of ProductPriceEntity
   */
  public ProductPriceMother defaultTemplate() {
    id = DEFAULT_ID;
    brandId = DEFAULT_BRAND_ID;
    currency = DEFAULT_CURRENCY;
    endDate = DEFAULT_END_DATE;
    price = DEFAULT_PRICE;
    priceIdentifier = DEFAULT_PRICE_IDENTIFIER;
    priority = DEFAULT_PRIORITY;
    productId = DEFAULT_PRODUCT_ID;
    startDate = DEFAULT_START_DATE;
    return this;
  }

  /**
   * priority one.
   */
  public void priorityOneTemplate() {
    id = DEFAULT_ID;
    brandId = DEFAULT_BRAND_ID;
    currency = DEFAULT_CURRENCY;
    endDate = LocalDateTime.of(2020, 6, 14, 15, 0);
    price = DEFAULT_PRICE;
    priceIdentifier = 2;
    priority = 1;
    productId = DEFAULT_PRODUCT_ID;
    startDate = LocalDateTime.of(2020, 6, 14, 18, 30);
  }

  // BUILD OBJECT
  public ProductPrice build() {
    //@off
    return ProductPrice.builder()
        .id(id)
        .brandId(brandId)
        .currency(currency)
        .endDate(endDate)
        .price(price)
        .priceIdentifier(priceIdentifier)
        .priority(priority)
        .productId(productId)
        .startDate(startDate)
        .build();
    //@on
  }
}
