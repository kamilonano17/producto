package com.inditex.product.infraestructure.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import com.inditex.product.business.dto.ProductPrice;
import com.inditex.product.infraestructure.dto.ProductPriceEntity;
import com.inditex.product.infraestructure.dto.ProductPriceEntityMother;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("ProductPriceEntityMapper tests")
class ProductPriceEntityMapperTest {

  private final ProductPriceEntityMapper sut = new ProductPriceEntityMapperImpl();


  @DisplayName("toProductPrice tests")
  @Nested
  class ToProductPriceTest {

    @DisplayName("GIVEN parameter is null WHEN map ProductPrice THEN expected null")
    @Test
    void givenParametersIsOk_whenMapProductPriceFindByDateCommand_thenExpectedNull() {

      // GIVEN
      ProductPriceEntity productPriceEntity = null;

      // WHEN
      ProductPrice result = sut.toProductPrice(productPriceEntity);

      // THEN
      assertThat(result).isNull();
    }

    @DisplayName("GIVEN parameter is null WHEN map ProductPrice THEN expected success map")
    @Test
    void givenParametersIsOk_whenMapProductPriceFindByDateCommand_thenExpectedSuccessMap() {

      // GIVEN
      ProductPriceEntity productPriceEntity = ProductPriceEntityMother.builder().defaultTemplate().build();

      // WHEN
      ProductPrice result = sut.toProductPrice(productPriceEntity);

      // THEN
      assertThat(result).usingRecursiveComparison().isEqualTo(productPriceEntity);
    }

  }
}
