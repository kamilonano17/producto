package com.inditex.product.infraestructure.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Optional;

import com.inditex.product.business.dto.ProductPrice;
import com.inditex.product.business.dto.ProductPriceMother;
import com.inditex.product.infraestructure.dto.ProductPriceEntity;
import com.inditex.product.infraestructure.dto.ProductPriceEntityMother;
import com.inditex.product.infraestructure.mapper.ProductPriceEntityMapper;
import com.inditex.product.infraestructure.repository.ProductPriceRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@DisplayName("ProductPriceService Tests")
@ExtendWith(MockitoExtension.class)
class ProductPriceServiceTest {

  @Mock
  private ProductPriceEntityMapper productPriceEntityMapper;

  @Mock
  private ProductPriceRepository productPriceRepository;

  @InjectMocks
  private ProductPriceService sut;


  @DisplayName("findProduct tests")
  @Nested
  class FindProductTest {
    @DisplayName("GIVEN product price not exist WHEN find in database THEN expected empty result")
    @Test
    void givenProductPriceNotExist_whenFindInDatabase_thenExpectedEmptyResult() {
      // GIVEN
      Integer productId = ProductPriceEntityMother.DEFAULT_PRODUCT_ID;
      Integer brandId = ProductPriceEntityMother.DEFAULT_BRAND_ID;
      LocalDateTime requestDate = ProductPriceEntityMother.DEFAULT_START_DATE;

      when(productPriceRepository.findByDate(brandId, productId, requestDate)).thenReturn(Optional.empty());

      // WHEN
      Optional<ProductPrice> result = sut.findProduct(productId, brandId, requestDate);

      // THEN
      assertThat(result).isEmpty();

    }

    @DisplayName("GIVEN product price exist WHEN find in database THEN expected result")
    @Test
    void givenProductPriceExist_whenFindInDatabase_thenExpectedResult() {
      // GIVEN
      Integer productId = ProductPriceEntityMother.DEFAULT_PRODUCT_ID;
      Integer brandId = ProductPriceEntityMother.DEFAULT_BRAND_ID;
      LocalDateTime requestDate = ProductPriceEntityMother.DEFAULT_START_DATE;
      Optional<ProductPriceEntity> productPriceEntityOpt = Optional.of(ProductPriceEntityMother.builder().defaultTemplate().build());
      Optional<ProductPrice> expected = Optional.of(ProductPriceMother.builder().defaultTemplate().build());

      when(productPriceRepository.findByDate(brandId, productId, requestDate)).thenReturn(productPriceEntityOpt);
      when(productPriceEntityMapper.toProductPrice(productPriceEntityOpt.get())).thenReturn(expected.get());

      // WHEN
      Optional<ProductPrice> result = sut.findProduct(productId, brandId, requestDate);

      // THEN
      assertThat(result).isNotEmpty().usingRecursiveComparison().isEqualTo(expected);

    }
  }
}
