package com.inditex.product.presentation.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ProductPriceResponseMother {
  // DEFAULT VALUES
  public static final Integer DEFAULT_PRODUCT_ID = 35455;
  public static final Integer DEFAULT_BRAND_ID = 1;
  public static final BigDecimal DEFAULT_PRICE = BigDecimal.valueOf(35.50);
  public static final LocalDateTime DEFAULT_REQUEST_DATE = LocalDateTime.of(2020, 6, 14, 0, 0, 0);

  // ATTRIBUTES
  private Integer productId;
  private Integer brandId;
  private BigDecimal price;
  private LocalDateTime requestDate;

  // CONSTRUCTOR
  public static ProductPriceResponseMother builder() {
    return new ProductPriceResponseMother();
  }

  // MODIFIERS
  public ProductPriceResponseMother withProductId(Integer productId) {
    this.productId = productId;
    return this;
  }

  public ProductPriceResponseMother withBrandId(Integer brandId) {
    this.brandId = brandId;
    return this;
  }

  public ProductPriceResponseMother withPrice(BigDecimal price) {
    this.price = price;
    return this;
  }

  public ProductPriceResponseMother withRequestDate(LocalDateTime requestDate) {
    this.requestDate = requestDate;
    return this;
  }

  // TEMPLATES OF MOTHER
  public ProductPriceResponseMother defaultTemplate() {
    productId = DEFAULT_PRODUCT_ID;
    brandId = DEFAULT_BRAND_ID;
    price = DEFAULT_PRICE;
    requestDate = DEFAULT_REQUEST_DATE;
    return this;
  }

  // BUILD OBJECT
  public ProductPriceResponse build() {
    //@off
    return ProductPriceResponse.builder()
        .productId(productId)
        .brandId(brandId)
        .price(price)
        .requestDate(requestDate)
        .build();
    //@on
  }



}
