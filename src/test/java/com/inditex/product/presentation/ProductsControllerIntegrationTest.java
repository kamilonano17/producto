package com.inditex.product.presentation;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


@DisplayName("ProductsController Integration Tests")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
class ProductsControllerIntegrationTest {

  final static DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

  @Autowired
  private MockMvc mvc;

  @Autowired
  private ObjectMapper objectMapper;

  @DisplayName("GIVEN product price exist WHEN find product price by date THEN expected price and status 200.")
  @Test
  void givenProductPriceExist_whenFindProductPriceByDate_thenExpectedPriceAndStatus200() throws Exception {
    Integer productId = 35455;
    Integer brandId = 1;
    LocalDateTime requestDate = LocalDateTime.of(2020, 6, 14, 10, 0, 0);
    mvc.perform(MockMvcRequestBuilders.get("/products/{productId}/brand/{brandId}/price", productId, brandId).contentType(MediaType.APPLICATION_JSON)
        .queryParam("request_date", TIME_FORMATTER.format(requestDate))).andDo(print()).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("product_id").value("35455"))
        .andExpect(jsonPath("brand_id").value("1")).andExpect(jsonPath("price").value("35.5"))
        .andExpect(jsonPath("request_date").value("2020-06-14T10:00:00"));
  }

  @DisplayName("GIVEN product price not exist WHEN find product by date THEN expected error api and status 404.")
  @Test
  void givenProductPriceNotExist_whenFindProductPriceByDate_thenExpectedErrorApiAndStatus404() throws Exception {
    Integer productId = 35457;
    Integer brandId = 1;
    LocalDateTime requestDate = LocalDateTime.of(2020, 6, 14, 10, 0, 0);
    mvc.perform(MockMvcRequestBuilders.get("/products/{productId}/brand/{brandId}/price", productId, brandId).contentType(MediaType.APPLICATION_JSON)
        .queryParam("request_date", TIME_FORMATTER.format(requestDate))).andDo(print()).andExpect(status().isNotFound())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("error_code").value("PRICE_NOT_FOUND"))
        .andExpect(jsonPath("error_message").value("Product price not found."));
  }
}
