package com.inditex.product.presentation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;

import com.inditex.product.business.dto.ProductPrice;
import com.inditex.product.business.dto.ProductPriceMother;
import com.inditex.product.business.exception.ProductPriceNotFoundException;
import com.inditex.product.business.port.presentation.ProductPriceFindServicePort;
import com.inditex.product.business.port.presentation.command.ProductPriceFindByDateCommand;
import com.inditex.product.business.port.presentation.command.ProductPriceFindByDateCommandMother;
import com.inditex.product.presentation.dto.ProductPriceResponse;
import com.inditex.product.presentation.dto.ProductPriceResponseMother;
import com.inditex.product.presentation.mapper.ProductPriceFindByDateCommandMapper;
import com.inditex.product.presentation.mapper.ProductPriceResponseMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@DisplayName("ProductsController Tests")
@ExtendWith(MockitoExtension.class)
class ProductsControllerTest {

  @Mock
  private ProductPriceFindServicePort productPriceFindServicePort;

  @Mock
  private ProductPriceResponseMapper productPriceResponseMapper;

  @Mock
  private ProductPriceFindByDateCommandMapper productPriceFindByDateCommandMapper;

  @InjectMocks
  private ProductsController sut;


  @DisplayName("getProductPrice tests")
  @Nested
  class GetProductPriceTest {

    @DisplayName("GIVEN product price not found WHEN call get product price THEN throw ProductPriceNotFoundException")
    @Test
    void givenProductPriceNotFound_whenCallGetProductPrice_thenThrowProductPriceNotFoundException() {
      // GIVEN
      Integer productId = ProductPriceFindByDateCommandMother.DEFAULT_PRODUCT_ID;
      Integer brandId = ProductPriceFindByDateCommandMother.DEFAULT_BRAND_ID;
      LocalDateTime requestDate = ProductPriceFindByDateCommandMother.DEFAULT_REQUEST_DATE;
      ProductPriceFindByDateCommand productPriceFindByDateCommand = ProductPriceFindByDateCommandMother.builder().defaultTemplate().build();
      ProductPriceNotFoundException expected = new ProductPriceNotFoundException("Product price not found.");

      when(productPriceFindByDateCommandMapper.toProductPriceFindByDateCommand(productId, brandId, requestDate))
          .thenReturn(productPriceFindByDateCommand);
      when(productPriceFindServicePort.findPriceByDate(productPriceFindByDateCommand)).thenThrow(expected);

      // WHEN
      ProductPriceNotFoundException result =
          catchThrowableOfType(() -> sut.getProductPrice(productId, brandId, requestDate), ProductPriceNotFoundException.class);

      // THEN
      assertThat(result).usingRecursiveComparison().isEqualTo(expected);

    }

    @DisplayName("GIVEN product not found WHEN call get product price THEN throw ProductPriceNotFoundException")
    @Test
    void givenProductPriceExist_whenCallGetProductPrice_thenExpectedPriceToApply() {
      // GIVEN
      Integer productId = ProductPriceFindByDateCommandMother.DEFAULT_PRODUCT_ID;
      Integer brandId = ProductPriceFindByDateCommandMother.DEFAULT_BRAND_ID;
      LocalDateTime requestDate = ProductPriceFindByDateCommandMother.DEFAULT_REQUEST_DATE;
      ProductPriceFindByDateCommand productPriceFindByDateCommand = ProductPriceFindByDateCommandMother.builder().defaultTemplate().build();
      ProductPrice productPrice = ProductPriceMother.builder().build();
      ProductPriceResponse expected = ProductPriceResponseMother.builder().defaultTemplate().build();

      when(productPriceFindByDateCommandMapper.toProductPriceFindByDateCommand(productId, brandId, requestDate))
          .thenReturn(productPriceFindByDateCommand);
      when(productPriceFindServicePort.findPriceByDate(productPriceFindByDateCommand)).thenReturn(productPrice);
      when(productPriceResponseMapper.toProductPriceResponse(productPrice, requestDate)).thenReturn(expected);

      // WHEN
      ProductPriceResponse result = sut.getProductPrice(productId, brandId, requestDate).getBody();

      // THEN
      assertThat(result).isNotNull().usingRecursiveComparison().isEqualTo(expected);

    }
  }

}
