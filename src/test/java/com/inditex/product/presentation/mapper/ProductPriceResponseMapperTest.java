package com.inditex.product.presentation.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import com.inditex.product.business.dto.ProductPrice;
import com.inditex.product.business.dto.ProductPriceMother;
import com.inditex.product.presentation.dto.ProductPriceResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("ProductPriceResponseMapper tests")
class ProductPriceResponseMapperTest {

  private final ProductPriceResponseMapper sut = new ProductPriceResponseMapperImpl();


  @DisplayName("toProductPriceResponse tests")
  @Nested
  class ToProductPriceResponseTest {
    @DisplayName("GIVEN parameters are ok WHEN map ProductPriceResponse THEN expected null")
    @Test
    void givenParametersAreNull_whenMapProductPriceFindByDateCommand_thenExpectedNull() {

      // GIVEN
      LocalDateTime requestDate = null;
      ProductPrice productPrice = null;

      // WHEN
      ProductPriceResponse result = sut.toProductPriceResponse(productPrice, requestDate);

      // THEN
      assertThat(result).isNull();

    }

    @DisplayName("GIVEN parameters are ok WHEN map ProductPriceResponse THEN expected successful map")
    @Test
    void givenParametersAreOk_whenMapProductPriceFindByDateCommand_thenExpectedSuccessfulMap() {

      // GIVEN
      LocalDateTime requestDate = LocalDateTime.now();
      ProductPrice productPrice = ProductPriceMother.builder().defaultTemplate().build();

      // WHEN
      ProductPriceResponse result = sut.toProductPriceResponse(productPrice, requestDate);

      // THEN
      //@off
      assertThat(result).isNotNull()
          .extracting("productId", "brandId", "price", "requestDate")
          .contains(productPrice.getProductId(), productPrice.getBrandId(), productPrice.getPrice(), requestDate);
      //@on

    }
  }
}
