package com.inditex.product.presentation.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import com.inditex.product.business.port.presentation.command.ProductPriceFindByDateCommand;
import com.inditex.product.business.port.presentation.command.ProductPriceFindByDateCommandMother;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

@DisplayName("ProductPriceFindByDateCommandMapper tests")
class ProductPriceFindByDateCommandMapperTest {

  private final ProductPriceFindByDateCommandMapper sut = new ProductPriceFindByDateCommandMapperImpl();

  @DisplayName("toProductPriceFindByDateCommand tests")
  @Nested
  class ToProductPriceFindByDateCommandTest {

    @DisplayName("GIVEN parameters are null WHEN map ProductPriceFindByDateCommand THEN expected null")
    @Test
    void givenParametersAreNull_whenMapProductPriceFindByDateCommand_thenExpectedNull() {

      // GIVEN
      Integer productId = null;
      Integer brandId = null;
      LocalDateTime requestDate = null;

      // WHEN
      ProductPriceFindByDateCommand result = sut.toProductPriceFindByDateCommand(productId, brandId, requestDate);

      // THEN
      assertThat(result).isNull();

    }

    @DisplayName("GIVEN parameters are ok WHEN map ProductPriceFindByDateCommand THEN expected success map")
    @Test
    void givenParametersAreOk_whenMapProductPriceFindByDateCommand_thenSuccessMap() {

      // GIVEN
      Integer productId = ProductPriceFindByDateCommandMother.DEFAULT_PRODUCT_ID;
      Integer brandId = ProductPriceFindByDateCommandMother.DEFAULT_BRAND_ID;
      LocalDateTime requestDate = ProductPriceFindByDateCommandMother.DEFAULT_REQUEST_DATE;
      ProductPriceFindByDateCommand expected = ProductPriceFindByDateCommandMother.builder().defaultTemplate().build();

      // WHEN
      ProductPriceFindByDateCommand result = sut.toProductPriceFindByDateCommand(productId, brandId, requestDate);

      // THEN
      assertThat(result).isNotNull().usingRecursiveComparison().isEqualTo(expected);

    }

  }

}
